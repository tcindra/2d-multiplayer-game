#include "default.h"

bool upArrow = false;
bool downArrow = false;
bool leftArrow = false;
bool rightArrow = false;
bool aKey = false;
bool dKey = false;

LRESULT CALLBACK MainWindowProcedure(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_DESTROY:
		{
			PostQuitMessage(0);
			return (0);
		}

	case WM_SIZING:
		{

		}

	case WM_MOUSEMOVE:
		{
			return (0);
		}

	case WM_KEYDOWN:
		{
			if (wParam == VK_UP || wParam == 0x57)
			{
				upArrow = true;
				return (0);
			}

			else if (wParam == VK_DOWN || wParam == 0x53)
			{
				downArrow = true;
				return (0);
			}

			else if (wParam == VK_LEFT)
			{
				leftArrow = true;
				return (0);
			}

			else if (wParam == VK_RIGHT)
			{
				rightArrow = true;
				return (0);
			}

			else if (wParam == 0x41)
			{
				aKey = true;
				return (0);
			}

			else if (wParam == 0x44)
			{
				dKey = true;
				return (0);
			}
		}

	case WM_KEYUP:
		{
			if (wParam == VK_UP || wParam == 0x57)
			{
				upArrow = false;
				return (0);
			}

			else if (wParam == VK_DOWN || wParam == 0x53)
			{
				downArrow = false;
				return (0);
			}

			else if (wParam == VK_LEFT)
			{
				leftArrow = false;
				return (0);
			}

			else if (wParam == VK_RIGHT)
			{
				rightArrow = false;
				return (0);
			}

			else if (wParam == 0x41)
			{
				aKey = false;
				return (0);
			}

			else if (wParam == 0x44)
			{
				dKey = false;
				return (0);
			}
		}
	}

	return DefWindowProc(hWnd, message, wParam, lParam);
}