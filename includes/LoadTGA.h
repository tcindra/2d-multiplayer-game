struct TGAFILE
{
	unsigned char header[12];
	unsigned short width;
	unsigned short height;
	unsigned char bits;
	unsigned char descriptor;
};

void LoadTGA(WCHAR * fileName);