#ifndef UNICODE
#define UNICODE
#endif

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <string>
#include <iostream>
#include <math.h>
#include <Windows.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <gl/GL.h>
#include <glext.h>
#include <wglext.h>
#include <regex>
#include <time.h>

#include "define.h"
#include "MainWindowProcedure.h"
#include "LoadTGA.h"
#include "structs_characters.h"
#include "NetworkProc.h"

extern SOCKET SetupSocket;
extern SOCKET TCPDataSocket;
extern SOCKET UDPDataSocket;

extern bool upArrow;
extern bool downArrow;
extern bool leftArrow;
extern bool rightArrow;
extern bool aKey;
extern bool dKey;

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "Ws2_32.lib")

