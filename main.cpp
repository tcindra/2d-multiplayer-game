#include "default.h"

LARGE_INTEGER ticksPerSecond   = {0};
LARGE_INTEGER hpcTimer1        = {0};
LARGE_INTEGER hpcTimer2        = {0};

GLfloat playerRichterX = 0.00f;
GLfloat playerRichterY = 0.00f;

sCHARACTER_LOCATION sAlucardLocation      = {-0.90f, 0.00f};
sCHARACTER_LOCATION sAlucardLocationOld   = {0};

TGAFILE * brickTextureDataInfo;
unsigned char * brickTextureData;

PFNGLCREATEPROGRAMPROC glCreateProgram;

// Vertex Array Objects
PFNGLGENVERTEXARRAYSPROC glGenVertexArrays;
PFNGLBINDVERTEXARRAYPROC glBindVertexArray;

//Vertex Buffer Objects
PFNGLGENBUFFERSPROC glGenBuffers;
PFNGLBINDBUFFERPROC glBindBuffer;
PFNGLBUFFERDATAPROC glBufferData;
PFNGLBUFFERSUBDATAPROC glBufferSubData;

PFNGLENABLEVERTEXATTRIBARRAYPROC glEnableVertexAttribArray;
PFNGLVERTEXATTRIBPOINTERPROC glVertexAttribPointer;
PFNGLCREATESHADERPROC glCreateShader;
PFNGLSHADERSOURCEPROC glShaderSource;
PFNGLCOMPILESHADERPROC glCompileShader;
PFNGLATTACHSHADERPROC glAttachShader;
PFNGLLINKPROGRAMPROC glLinkProgram;
PFNGLUSEPROGRAMPROC glUseProgram;
PFNGLGETSHADERIVPROC glGetShaderiv;
PFNGLDISABLEVERTEXATTRIBARRAYPROC glDisableVertexAttribArray;

// Uniforms
PFNGLGETUNIFORMLOCATIONPROC glGetUniformLocation;
PFNGLUNIFORM1IPROC glUniform1i;
PFNGLUNIFORM1FPROC glUniform1f;
PFNGLUNIFORM2FPROC glUniform2f;
PFNGLUNIFORMMATRIX4FVPROC glUniformMatrix4fv;

// Textures
PFNGLACTIVETEXTUREPROC glActiveTexture;

///////////////////////////////////////////////////////////////

LARGE_INTEGER qpcFreq = {0};
LARGE_INTEGER qpcCount1 = {0};
LARGE_INTEGER qpcCount2 = {0};

////////////////////////////////////////////////////
// Create Shaders //////////////////////////////////
////////////////////////////////////////////////////

GLuint shaders[2];

GLuint shaderProgram;

// Alucard Textures
GLuint Texture_Alucard_Run[31];

// Level 1 Textures
GLuint Texture_Background_ForestScene_CastleEntranceGate;
GLuint Texture_Background_ForestScene_CastleHallway;

GLint uniTexture;

void CreateShaders()
{
	const std::string SimpleVertexShader(
		"#version 330\n"
		"layout(location = 0) in vec4 position;\n"
		"layout(location = 1) in vec2 texCoords;\n"
		"out vec2 fragTexCoords;\n"
		"void main()\n"
		"{\n"
		"	gl_Position = position;\n"
		"	fragTexCoords = texCoords;\n"
		"}\n"
	);

	const std::string SimpleFragmentShader(
		"#version 330\n"
		"in vec2 fragTexCoords;\n"
		"uniform sampler2D theTexture;\n"
		"void main()\n"
		"{\n"
		"	vec4 texel = texture2D(theTexture, fragTexCoords);\n"
		"	if (texel.a < 0.5)\n"
		"	{\n"
		"		discard;\n"
		"	}\n"
		"	gl_FragColor = texel;\n"
		"}\n"
	);

	GLint status;

	shaders[0] = glCreateShader(GL_VERTEX_SHADER);
	const char * svShader = SimpleVertexShader.c_str();
	glShaderSource(shaders[0], 1, &svShader, NULL);
	glCompileShader(shaders[0]);
	glGetShaderiv(shaders[0], GL_COMPILE_STATUS, &status);

	if (status == GL_FALSE)
	{
		MessageBox(NULL, L"Shader Compile Error!", NULL, MB_OK);
	}

	shaders[1] = glCreateShader(GL_FRAGMENT_SHADER);
	const char * sfShader = SimpleFragmentShader.c_str();
	glShaderSource(shaders[1], 1, &sfShader, NULL);
	glCompileShader(shaders[1]);
	glGetShaderiv(shaders[1], GL_COMPILE_STATUS, &status);

	if (status == GL_FALSE)
	{
		MessageBox(NULL, L"Shader Compile Error!", NULL, MB_OK);
	}

	shaderProgram = glCreateProgram();

	for (int i = 0; i < sizeof(shaders); i++)
	{
		glAttachShader(shaderProgram, shaders[i]);
	}

	glLinkProgram(shaderProgram);

	// Alucard Texture Loading

	// Running
	glActiveTexture(GL_TEXTURE0);
	glGenTextures(31, Texture_Alucard_Run);
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[0]);
	LoadTGA(L"sotn/alucard-sprites/run-0.tga");
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[1]);
	LoadTGA(L"sotn/alucard-sprites/run-1.tga");
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[2]);
	LoadTGA(L"sotn/alucard-sprites/run-2.tga");
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[3]);
	LoadTGA(L"sotn/alucard-sprites/run-3.tga");
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[4]);
	LoadTGA(L"sotn/alucard-sprites/run-4.tga");
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[5]);
	LoadTGA(L"sotn/alucard-sprites/run-5.tga");
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[6]);
	LoadTGA(L"sotn/alucard-sprites/run-6.tga");
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[7]);
	LoadTGA(L"sotn/alucard-sprites/run-7.tga");
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[8]);
	LoadTGA(L"sotn/alucard-sprites/run-8.tga");
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[9]);
	LoadTGA(L"sotn/alucard-sprites/run-9.tga");
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[10]);
	LoadTGA(L"sotn/alucard-sprites/run-10.tga");
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[11]);
	LoadTGA(L"sotn/alucard-sprites/run-11.tga");
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[12]);
	LoadTGA(L"sotn/alucard-sprites/run-12.tga");
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[13]);
	LoadTGA(L"sotn/alucard-sprites/run-13.tga");
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[14]);
	LoadTGA(L"sotn/alucard-sprites/run-14.tga");
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[15]);
	LoadTGA(L"sotn/alucard-sprites/run-15.tga");
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[16]);
	LoadTGA(L"sotn/alucard-sprites/run-16.tga");
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[17]);
	LoadTGA(L"sotn/alucard-sprites/run-17.tga");
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[18]);
	LoadTGA(L"sotn/alucard-sprites/run-18.tga");
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[19]);
	LoadTGA(L"sotn/alucard-sprites/run-19.tga");
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[20]);
	LoadTGA(L"sotn/alucard-sprites/run-20.tga");
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[21]);
	LoadTGA(L"sotn/alucard-sprites/run-21.tga");
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[22]);
	LoadTGA(L"sotn/alucard-sprites/run-22.tga");
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[23]);
	LoadTGA(L"sotn/alucard-sprites/run-23.tga");
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[24]);
	LoadTGA(L"sotn/alucard-sprites/run-24.tga");
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[25]);
	LoadTGA(L"sotn/alucard-sprites/run-25.tga");
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[26]);
	LoadTGA(L"sotn/alucard-sprites/run-26.tga");
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[27]);
	LoadTGA(L"sotn/alucard-sprites/run-27.tga");
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[28]);
	LoadTGA(L"sotn/alucard-sprites/run-28.tga");
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[29]);
	LoadTGA(L"sotn/alucard-sprites/run-29.tga");
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[30]);
	LoadTGA(L"sotn/alucard-sprites/run-30.tga");
	glActiveTexture(0);

	// Level 1: Forest Scene Texture Loading
	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &Texture_Background_ForestScene_CastleEntranceGate);

	glBindTexture(GL_TEXTURE_2D, Texture_Background_ForestScene_CastleEntranceGate);
	LoadTGA(L"sotn/backgrounds/forest-scene/castle-gate-entrance.tga");
	glBindTexture(GL_TEXTURE_2D, Texture_Background_ForestScene_CastleHallway);
	LoadTGA(L"sotn/backgrounds/forest-scene/castle-hallway.tga");
	glActiveTexture(0);


	glActiveTexture(0);
	
	uniTexture = glGetUniformLocation(shaderProgram, "theTexture");

	glLinkProgram(0);
	
}

GLuint vaoAlucard;
GLuint vaoRichter;
GLuint vaoBrickFloor1;
GLuint vaoBackgroundCastleEntranceGate;
GLuint vaoBackgroundCastleHallway;

GLfloat fAlucardX = 0.10f;
GLfloat fAlucardY = 0.00f;

int runPosition = 0;
int runPositionRev = false;

double fps = 30;

void InitializeProgram()
{
	CreateShaders();
}

float levelPosition = 0;

void Draw(HDC hDC)
{
	QueryPerformanceCounter(&hpcTimer1);

	glClearColor(0.05f, 0.0f, 0.05f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable(GL_DEPTH_TEST);
	
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	if (aKey)
	{
		sAlucardLocation.x = sAlucardLocation.x - (0.25f / (1.00 / fps));
	}

	int runPositionPrev = 0;

	if (dKey)
	{
		sAlucardLocation.x = sAlucardLocation.x + (0.25f / (1.00 / fps));

		if (runPosition == (30 * 2.2 * (1.00/ceil(fps))))
		{
			runPosition = 15 * 2.2 * (1.00/ceil(fps));
			runPositionPrev = 14 * 2.2 * (1.00/ceil(fps));
		}

		else if (runPosition < (30 * 2.2 * (1.00/ceil(fps))))
		{
			runPosition = runPosition + 1;
		}
	}

	else
	{
		runPosition = 0;
		runPositionRev = false;
	}

	if (fAlucardX > (0.99f + (0.25f * 2.2/ (1.00 / fps))))
	{
		//levelPosition = levelPosition + 2;
		//playerAlucardX = 0.00f;
	}

	GLfloat playerAlucardVertices[] = 
	{sAlucardLocation.x, sAlucardLocation.y + -0.72f, -0.50f, 1.00f,
	 sAlucardLocation.x + 0.10f, sAlucardLocation.y + -0.72f, -0.50f, 1.00f,
	 sAlucardLocation.x + 0.10f, sAlucardLocation.y + -0.52f, -0.50f, 1.00f,
	 sAlucardLocation.x, sAlucardLocation.y + -0.52f, -0.50f, 1.00f};

	GLfloat playerAlucardColor[] = 
	{1.00f, 0.00f, 0.00f, 1.00f,
	 1.00f, 0.00f, 0.00f, 1.00f,
	 1.00f, 0.00f, 0.00f, 1.00f};

	GLfloat playerAlucardTexture[] = 
	{0.0, 0.0,
	 1.0, 0.0,
	 1.0, 1.0,
	 0.0, 1.0};

	GLfloat playerRichterVertices[] = 
	{playerRichterX + 0.20f, playerRichterY + 0.00f, -0.50f, 1.00f,
	 playerRichterX + 0.30f, playerRichterY + 0.00f, -0.50f, 1.00f,
	 playerRichterX + 0.25f, playerRichterY + 0.10f, -0.50f, 1.00f};

	GLfloat playerRichterTexture[] = 
	{0.0, 0.0,
	 1.0, 0.0,
	 1.0, 1.0,
	 0.0, 1.0};

	GLfloat backgroundCastleEntranceGateVertices[] = 
	{-1.00f - levelPosition, -1.00f, -0.50f, 1.00f,
	 1.00f - levelPosition, -1.00f, -0.50f, 1.00f,
	 1.00f - levelPosition, 2.00f, -0.50f, 1.00f,
	 -1.00f - levelPosition, 2.00f, -0.50f, 1.00f};

	GLfloat backgroundCastleEntranceGateTexture[]  = 
	{0.00f, 0.00f,
	 0.95f, 0.00f,
	 0.95f, 1.00f,
	 0.00f, 1.00f};

	GLfloat backgroundCastleHallwayVertices[] = 
	{1.00f - levelPosition, -1.00f, -0.50f, 1.00f,
	 3.00f - levelPosition, -1.00f, -0.50f, 1.00f,
	 3.00f - levelPosition, 0.00f, -0.50f, 1.00f,
	 1.00f - levelPosition, 0.00f, -0.50f, 1.00f};

	GLfloat backgroundCastleHallwayTexture[] = 
	{0.00f, 0.00f,
	 0.95f, 0.00f,
	 0.95f, 1.00f,
	 0.00f, 1.00f};

	if (((sAlucardLocation.x != sAlucardLocationOld.x) || (sAlucardLocation.y != sAlucardLocationOld.y)))
	{
		CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) NetworkProc, (LPVOID) &sAlucardLocation, 0, NULL);

		sAlucardLocationOld.x = sAlucardLocation.x;
		sAlucardLocationOld.y = sAlucardLocation.y;
	}

	GLuint vboAlucard[2];
	glGenBuffers(2, vboAlucard);

	glBindBuffer(GL_ARRAY_BUFFER, vboAlucard[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(playerAlucardVertices), &playerAlucardVertices, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, vboAlucard[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(playerAlucardTexture), &playerAlucardTexture, GL_STATIC_DRAW);

	GLuint vboRichter[2];
	glGenBuffers(2, vboRichter);

	glBindBuffer(GL_ARRAY_BUFFER, vboRichter[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(playerRichterVertices), &playerRichterVertices, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, vboRichter[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(playerRichterTexture), &playerRichterTexture, GL_STATIC_DRAW);

	GLuint vboBackgroundCastleEntranceGate[2];
	glGenBuffers(2, vboBackgroundCastleEntranceGate);

	glBindBuffer(GL_ARRAY_BUFFER, vboBackgroundCastleEntranceGate[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(backgroundCastleEntranceGateVertices), &backgroundCastleEntranceGateVertices, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, vboBackgroundCastleEntranceGate[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(backgroundCastleEntranceGateTexture), &backgroundCastleEntranceGateTexture, GL_STATIC_DRAW);

	GLuint vboBackgroundCastleHallway[2];
	glGenBuffers(2, vboBackgroundCastleHallway);

	glBindBuffer(GL_ARRAY_BUFFER, vboBackgroundCastleHallway[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(backgroundCastleHallwayVertices), &backgroundCastleHallwayVertices, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, vboBackgroundCastleHallway[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(backgroundCastleHallwayTexture), &backgroundCastleHallwayTexture, GL_STATIC_DRAW);

	glUseProgram(shaderProgram);

	int runFrame = runPosition / (2.2 * 1.00 / ceil(fps));

	glGenVertexArrays(1, &vaoAlucard);
	glBindVertexArray(vaoAlucard);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, vboAlucard[0]);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, vboAlucard[1]);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glBindVertexArray(0);

	glGenVertexArrays(1, &vaoBackgroundCastleEntranceGate);
	glBindVertexArray(vaoBackgroundCastleEntranceGate);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, vboBackgroundCastleEntranceGate[0]);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, vboBackgroundCastleEntranceGate[1]);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glBindVertexArray(0);

	glGenVertexArrays(1, &vaoBackgroundCastleHallway);
	glBindVertexArray(vaoBackgroundCastleHallway);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, vboBackgroundCastleHallway[0]);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, vboBackgroundCastleHallway[1]);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glBindVertexArray(0);

	// Drawing Calls

	glBindVertexArray(vaoAlucard);
	glBindTexture(GL_TEXTURE_2D, Texture_Alucard_Run[(runFrame)]);
	glDrawArrays(GL_QUADS, 0, 4);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);

	glBindVertexArray(vaoBackgroundCastleEntranceGate);
	glBindTexture(GL_TEXTURE_2D, Texture_Background_ForestScene_CastleEntranceGate);
	glDrawArrays(GL_QUADS, 0, 4);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);

	glBindVertexArray(vaoBackgroundCastleHallway);
	glBindTexture(GL_TEXTURE_2D, Texture_Background_ForestScene_CastleHallway);
	glDrawArrays(GL_QUADS, 0, 4);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);

	glUseProgram(0);

	glViewport(0, 0, 1400, 800);

	SwapBuffers(hDC);

	QueryPerformanceCounter(&hpcTimer2);

	fps = ((double)(((double)((hpcTimer2.QuadPart - hpcTimer1.QuadPart))) / ((double)(ticksPerSecond.QuadPart))));
}

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR pCmdLine, int nCmdShow)
{
	if (!QueryPerformanceFrequency(&ticksPerSecond))
	{
		MessageBox(NULL, L"Hardware does not support high-resolution performance counter!", NULL, MB_OK);
		return (1);
	}

	WNDCLASS MainWindowClass = {};

	MainWindowClass.lpfnWndProc   = MainWindowProcedure;
	MainWindowClass.hInstance     = hInstance;
	MainWindowClass.lpszClassName = L"2D Multiplayer Game";
	MainWindowClass.hCursor       = LoadCursor(0, IDC_ARROW);

	RegisterClass(&MainWindowClass);

	RECT rect;
	SetRect(&rect, 0, 0, 1400, 800);

	AllocConsole();
	AttachConsole(ATTACH_PARENT_PROCESS);

	HWND hConsoleWindow = GetConsoleWindow();

	SetConsoleTitle(L"Debug");
	MoveWindow(hConsoleWindow, 1460, 120, 400, 600, TRUE);
	freopen("CON", "w", stdout);

	std::cout << "--- Client Area --- \n";
	std::cout << "Width: 1400\n";
	std::cout << "Height: 800\n\n";
	std::cout << "--- Non-Client Area --- \n";
	std::cout << "Width: " << (rect.right - rect.left) << "\n";
	std::cout << "Height: " << (rect.bottom - rect.top) << "\n\n";
	
	HWND hMainWindow = CreateWindowEx(CS_OWNDC, L"2D Multiplayer Game", L"2D Multiplayer Game", WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX, 20, 120, rect.right - rect.left, rect.bottom - rect.top, NULL, NULL, hInstance, NULL);

	if (hMainWindow == NULL)
	{
		return (1);
	}

	ShowWindow(hMainWindow, nCmdShow);

	HDC hMainWindowDC = GetDC(hMainWindow);

	PIXELFORMATDESCRIPTOR pfd = {0};

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;

	pfd.dwFlags = PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER | PFD_DRAW_TO_WINDOW;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 24;
	pfd.cDepthBits = 32;

	int DelegatedPixelFormat = ChoosePixelFormat(hMainWindowDC, &pfd);

	if (DelegatedPixelFormat == 0)
	{
		return (2);
	}

	int SetPixelFormatResult = SetPixelFormat(hMainWindowDC, DelegatedPixelFormat, &pfd);

	if (SetPixelFormatResult == NULL)
	{
		return (3);
	}

	HGLRC hMainWindowGLRC = wglCreateContext(hMainWindowDC);

	wglMakeCurrent(hMainWindowDC, hMainWindowGLRC);

	glCreateProgram             = (PFNGLCREATEPROGRAMPROC) wglGetProcAddress("glCreateProgram");
	glGenVertexArrays           = (PFNGLGENVERTEXARRAYSPROC) wglGetProcAddress("glGenVertexArrays");
	glBindVertexArray           = (PFNGLBINDVERTEXARRAYPROC) wglGetProcAddress("glBindVertexArray");
	glGenBuffers                = (PFNGLGENBUFFERSPROC) wglGetProcAddress("glGenBuffers");
	glBindBuffer                = (PFNGLBINDBUFFERPROC) wglGetProcAddress("glBindBuffer");
	glBufferData                = (PFNGLBUFFERDATAPROC) wglGetProcAddress("glBufferData");
	glBufferSubData             = (PFNGLBUFFERSUBDATAPROC) wglGetProcAddress("glBufferSubData");
	glEnableVertexAttribArray   = (PFNGLENABLEVERTEXATTRIBARRAYPROC) wglGetProcAddress("glEnableVertexAttribArray");
	glVertexAttribPointer       = (PFNGLVERTEXATTRIBPOINTERPROC) wglGetProcAddress("glVertexAttribPointer");
	glCreateShader              = (PFNGLCREATESHADERPROC) wglGetProcAddress("glCreateShader");
	glShaderSource              = (PFNGLSHADERSOURCEPROC) wglGetProcAddress("glShaderSource");
	glCompileShader             = (PFNGLCOMPILESHADERPROC) wglGetProcAddress("glCompileShader");
	glAttachShader              = (PFNGLATTACHSHADERPROC) wglGetProcAddress("glAttachShader");
	glLinkProgram               = (PFNGLLINKPROGRAMPROC) wglGetProcAddress("glLinkProgram");
	glUseProgram                = (PFNGLUSEPROGRAMPROC) wglGetProcAddress("glUseProgram");
	glGetShaderiv               = (PFNGLGETSHADERIVPROC) wglGetProcAddress("glGetShaderiv");
	glGenVertexArrays           = (PFNGLGENVERTEXARRAYSPROC) wglGetProcAddress("glGenVertexArrays");
	glBindVertexArray           = (PFNGLBINDVERTEXARRAYPROC) wglGetProcAddress("glBindVertexArray");
	glDisableVertexAttribArray  = (PFNGLDISABLEVERTEXATTRIBARRAYPROC) wglGetProcAddress("glDisableVertexAttribArray");
	glGetUniformLocation        = (PFNGLGETUNIFORMLOCATIONPROC) wglGetProcAddress("glGetUniformLocation");
	glUniform1i                 = (PFNGLUNIFORM1IPROC) wglGetProcAddress("glUniform1i");
	glUniform1f                 = (PFNGLUNIFORM1FPROC) wglGetProcAddress("glUniform1f");
	glUniform2f                 = (PFNGLUNIFORM2FPROC) wglGetProcAddress("glUniform2f");
	glUniformMatrix4fv          = (PFNGLUNIFORMMATRIX4FVPROC) wglGetProcAddress("glUniformMatrix4fv");
	glActiveTexture             = (PFNGLACTIVETEXTUREPROC) wglGetProcAddress("glActiveTexture");

	MSG message = {};

	WSADATA wsaData;

	int iResult = WSAStartup(MAKEWORD(2,2), &wsaData);

	addrinfo * result = NULL;
	addrinfo UDPHints = {0};

	UDPHints.ai_protocol = IPPROTO_UDP;
	UDPHints.ai_socktype = SOCK_DGRAM;
	UDPHints.ai_family = AF_INET;

	iResult = getaddrinfo("127.0.0.1", DEFAULT_PORT, &UDPHints, &result);

	if (iResult != 0)
	{
		std::cout << "getaddrinfo(...) failed! Error Code: " << iResult << "\n";
		return (1);
	}

	UDPDataSocket = socket((*result).ai_family, (*result).ai_socktype, (*result).ai_protocol);

	if (UDPDataSocket == INVALID_SOCKET)
	{
		std::cout << "socket(...) failed!\n";
		return (2);
	}

	iResult = connect(UDPDataSocket, (*result).ai_addr, (int) (*result).ai_addrlen);

	if (iResult == SOCKET_ERROR)
	{
		std::cout << "connect(...) failed!\n";
		return (2);
	}


	InitializeProgram();

	while (1)
	{
		if (PeekMessage(&message, NULL, 0, 0, PM_REMOVE))
		{
			if (message.message == WM_QUIT)
			{
				break;
			}

			TranslateMessage(&message);
			DispatchMessage(&message);
		}
		
		Draw(hMainWindowDC);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(hMainWindowGLRC);

	ReleaseDC(hMainWindow, hMainWindowDC);

	return (0);
}