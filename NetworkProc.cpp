#include "default.h"

SOCKET SetupSocket   = INVALID_SOCKET;
SOCKET TCPDataSocket = INVALID_SOCKET;
SOCKET UDPDataSocket = INVALID_SOCKET;

DWORD WINAPI NetworkProc(sCHARACTER_LOCATION sCharacter1Location)
{

	int iResult = send(UDPDataSocket, (char *) &sCharacter1Location, sizeof(sCharacter1Location), 0);

	if (iResult == SOCKET_ERROR)
	{
		std::cout << "send(...) failed! Error Code: " << WSAGetLastError() << "\n";
		return (3);
	}

	return (0);
}