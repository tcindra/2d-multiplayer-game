#include "default.h"

void LoadTGA(WCHAR * fileName)
{
	TGAFILE tgaFile = {};

	HANDLE hFile = CreateFile(fileName, GENERIC_READ, 0, NULL, OPEN_EXISTING, NULL, NULL);

	if (hFile == INVALID_HANDLE_VALUE)
	{
		std::wcout << "Could not open \"" << fileName << "\"\n";
		return;
	}

	//unsigned char buffer[18];
	DWORD bytesRead = 0;

	int currentPosition = SetFilePointer(hFile, 0, NULL, FILE_BEGIN);

	ReadFile(hFile, tgaFile.header, sizeof(tgaFile.header), &bytesRead, NULL);
	ReadFile(hFile, &(tgaFile.width), sizeof(tgaFile.width), &bytesRead, NULL);
	ReadFile(hFile, &(tgaFile.height), sizeof(tgaFile.height), &bytesRead, NULL);
	ReadFile(hFile, &(tgaFile.bits), sizeof(tgaFile.bits), &bytesRead, NULL);
	ReadFile(hFile, &(tgaFile.descriptor), sizeof(tgaFile.descriptor), &bytesRead, NULL);

	//std::cout << "Width: " << tgaFile.width << "\n";
	//std::cout << "Height: " << tgaFile.height << "\n";
	//std::cout << "Bits: " << (int) tgaFile.bits << "\n";
	//std::cout << "Descriptor: " << (int) tgaFile.descriptor << "\n\n";

	int imageDataSize = tgaFile.width * tgaFile.height * (((int) tgaFile.bits) / 8);

	unsigned char * data = new unsigned char[imageDataSize];

	ReadFile(hFile, data, imageDataSize, &bytesRead, NULL);

	if (bytesRead == imageDataSize)
	{
		for (int iter = 0; iter < imageDataSize; iter = iter + ((int) tgaFile.bits / 8))
		{
			unsigned char temp = data[iter];
			data[iter] = data[iter + 2];
			data[iter + 2] = temp;
		}

		std::wcout << "\"" << fileName << "\" loaded successfully!\n";
		
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tgaFile.width, tgaFile.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

		return;
	}
}